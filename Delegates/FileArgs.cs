﻿namespace Delegates;

public sealed class FileArgs : EventArgs
{
    #region Public Properties

    public string? FileName { get; set; }

    #endregion
}
