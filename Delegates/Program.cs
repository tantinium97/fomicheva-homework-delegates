﻿using Delegates.Extensions;

namespace Delegates;

static class Program
{
    #region Private Methods

    static void Main()
    {
        var settings = new Settings();

        int workingTime = settings.workingTime;
        string path = settings.path;

        CancellationTokenSource source = new(workingTime);

        var fileExplorer = new FileExplorer(path);

        fileExplorer.FileFound += (s, e) =>
        {
            Console.WriteLine("Обнаружен файл: {0}", e.FileName);
        };

        fileExplorer.Explore(source);

        var files = Directory.GetFiles(path);
        var fileLongestName = files.GetMax<string>(f => f.Length);
        Console.WriteLine(files.Length > 0 ? $"Самое длинное название: {Path.GetFileName(fileLongestName)}" : "Директория пуста");
    }

    #endregion;
}

