﻿namespace Delegates;

public sealed class FileExplorer
{
    #region Private Fields

    private readonly string _path;

    #endregion

    #region Public Events

    public event EventHandler<FileArgs>? FileFound;

    #endregion

    #region Public Constructors

    public FileExplorer(string path)
    {
        _path = path;
    }

    #endregion

    #region Public Methods

    public void Explore(CancellationTokenSource tokenSource)
    {
        foreach (var file in Directory.GetFiles(_path))
        {
            if (tokenSource.IsCancellationRequested)
            {
                Console.WriteLine("Время работы вышло. Поиск файлов остановлен");
                return;    
            }
            FileFound?.Invoke(this, new FileArgs { FileName = Path.GetFileName(file) });
        }
    }

    #endregion
}
