﻿using System.Collections;

namespace Delegates.Extensions;

public static class MyExtension
{
    #region Public Methods

    public static T? GetMax<T>(this IEnumerable e, Func<T,float> getParameter) where T : class
    {
        var maxValue = float.MinValue;

        T? maxElement = default;
        
        foreach (T element in e)
        {
            var value = getParameter(element);

            if (value > maxValue)
            {
                maxValue = value;
                maxElement = element;
            }
        }

        return maxElement;
    }

    #endregion
}
